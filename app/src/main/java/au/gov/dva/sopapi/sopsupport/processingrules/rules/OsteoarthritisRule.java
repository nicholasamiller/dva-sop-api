package au.gov.dva.sopapi.sopsupport.processingrules.rules;

import au.gov.dva.sopapi.interfaces.ConditionConfiguration;

public class OsteoarthritisRule extends LumbarSpondylosisRule {
    public OsteoarthritisRule(ConditionConfiguration conditionConfiguration) {
        super(conditionConfiguration);
    }
}
