package au.gov.dva.sopapi.sopref.parsing.implementations.parsers

import au.gov.dva.sopapi.dtos.StandardOfProof
import au.gov.dva.sopapi.sopref.parsing.implementations.model.FactorInfo
import au.gov.dva.sopapi.sopref.parsing.traits.{PreAug2015FactorsParser, PreAugust2015SoPParser}

import scala.collection.immutable.Seq

object OsteoarthritisParser$PreAug2015 extends PreAugust2015SoPParser {
}
